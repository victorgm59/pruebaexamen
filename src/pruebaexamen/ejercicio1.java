package pruebaexamen;

public class ejercicio1 {
	
	public static enum Estado {
		SANA("RRRRRRRRR"),
		APATICA("PUF"), 
		ENFERMA("AY"), 
		FALLECIDA("LA MASCOTA HA FALLECIDO");
		String descripcion;
		private Estado(String descripcion) {
			this.descripcion = descripcion;
		}
		public String getDescripcion() {
			return descripcion;
		}
	} 
	
	private String nombre;
	private int energia = 20;
	
	public ejercicio1(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	public String comer() {
		Estado e = estado();
		if (e == Estado.FALLECIDA)
			new Exception("La mascota ha fallecido");
		return nombre;
		
	}

	private Estado estado() {
		if (energia > 55 || energia < 0) 
			return Estado.FALLECIDA;
		else if (energia < 5 || energia > 50)
			return Estado.ENFERMA;
		else if (energia < 7 || energia > 47)
			return Estado.APATICA;
		else 
			return Estado.SANA;	
	}
}
