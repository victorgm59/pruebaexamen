package pruebaexamen;

public class revista extends publicacion{
	
	private int dia;
	private int mes;
	
	public revista(int codigo, String titulo, int anyo, int mes, int dia) {
		super(codigo, titulo, anyo);
		this.mes = mes;
		this.dia = dia;
	}
	public int getDia() {
		return dia;
	}
	public int getMes() {
		return mes;
	}
	@Override
	public String toString() {
		return "Revista " + super.toString();
	}
	
	
}
