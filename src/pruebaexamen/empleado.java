package pruebaexamen;

public class empleado {
	private String nombre;
	private String apellidos;
	private String fecha;
	private String numero_cuenta;
	
	public empleado(String nombre, String apellidos, String fecha, String cuenta) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fecha = fecha;
		this.numero_cuenta = cuenta;
	}

	public String getNumero_cuenta() {
		return numero_cuenta;
	}

	public void setNumero_cuenta(String numero_cuenta) {
		this.numero_cuenta = numero_cuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getFecha() {
		return fecha;
	}

	@Override
	public String toString() {
		return "Nombre: " + nombre + " apellidos: " + apellidos + " fecha: " + fecha + " numero de cuenta: " + numero_cuenta;
	}
	
	
}
